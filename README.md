# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Installation

To install and run this project, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Install the dependencies using `npm install`.
4. add .env file in your root directory and add backend url in variable named VITE_SOME_BACKEND_URL 
4. Start the development server using `npm run dev`.
5. Open your browser and navigate to `http://localhost:5173`.

## Description

This application fetches data from a Go backend and filters the search results to display a list of products in different categories and locations. The frontend is built using React and Vite, and includes some ESLint rules for code quality.